package com.example.bancodequestoes.model.dto;

public class CategoriaDTO {

    private String descricao;

    public CategoriaDTO() {
    }

    public CategoriaDTO(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
