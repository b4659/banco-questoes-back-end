package com.example.bancodequestoes.model;

import com.example.bancodequestoes.model.typeEnum.EDificuldade;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "questoes")
public class Questoes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "questao_dissertativa")
    private String questaoDissertativa;

    @Column(name = "questao_objetiva")
    private String questaoObjetiva;

    @Enumerated(EnumType.STRING)
    @Column(name = "nivel_dificuldade")
    private EDificuldade dificuldade;

    @ManyToOne
    @JoinColumn(name = "fk_usuario")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "fk_categoria")
    private Categoria categoria;

    @Column(name = "anexos")
    private String anexos;

    @CreatedDate
    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;

    @LastModifiedDate
    @Column(name = "data_atualizacao")
    private LocalDateTime dataAtualizacao;

    @Column(name = "deletado")
    private boolean deletado;

    public Questoes() {
    }

    public Questoes(Long id, String questaoDissertativa, String questaoObjetiva, EDificuldade dificuldade, Usuario usuario, Categoria categoria, String anexos, LocalDateTime dataCadastro, LocalDateTime dataAtualizacao, boolean deletado) {
        Id = id;
        this.questaoDissertativa = questaoDissertativa;
        this.questaoObjetiva = questaoObjetiva;
        this.dificuldade = dificuldade;
        this.usuario = usuario;
        this.categoria = categoria;
        this.anexos = anexos;
        this.dataCadastro = dataCadastro;
        this.dataAtualizacao = dataAtualizacao;
        this.deletado = deletado;
    }

    public Questoes(Long id, String questaoDissertativa, String questaoObjetiva, EDificuldade dificuldade, Usuario usuario, Categoria categoria, String anexos) {
        Id = id;
        this.questaoDissertativa = questaoDissertativa;
        this.questaoObjetiva = questaoObjetiva;
        this.dificuldade = dificuldade;
        this.usuario = usuario;
        this.categoria = categoria;
        this.anexos = anexos;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getQuestaoDissertativa() {
        return questaoDissertativa;
    }

    public void setQuestaoDissertativa(String questaoDissertativa) {
        this.questaoDissertativa = questaoDissertativa;
    }

    public String getQuestaoObjetiva() {
        return questaoObjetiva;
    }

    public void setQuestaoObjetiva(String questaoObjetiva) {
        this.questaoObjetiva = questaoObjetiva;
    }

    public EDificuldade getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(EDificuldade dificuldade) {
        this.dificuldade = dificuldade;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getAnexos() {
        return anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    public LocalDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public LocalDateTime getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public boolean isDeletado() {
        return deletado;
    }

    public void setDeletado(boolean deletado) {
        this.deletado = deletado;
    }

    @Override
    public String toString() {
        return "Questoes{" +
                "Id=" + Id +
                ", questaoDissertativa='" + questaoDissertativa + '\'' +
                ", questaoObjetiva='" + questaoObjetiva + '\'' +
                ", dificuldade=" + dificuldade +
                ", usuario=" + usuario +
                ", categoria=" + categoria +
                ", anexos='" + anexos + '\'' +
                ", dataCadastro=" + dataCadastro +
                ", dataAtualizacao=" + dataAtualizacao +
                ", deletado=" + deletado +
                '}';
    }
}
