package com.example.bancodequestoes.model.dto;

import com.example.bancodequestoes.model.typeEnum.EAlternativa;

public class RespostasDTO {

    private String texto;
    private EAlternativa alternativa;
    private Boolean correta;
    private Boolean verdadeiroFalso;

    public RespostasDTO() {
    }

    public RespostasDTO(String texto, EAlternativa alternativa, Boolean correta, Boolean verdadeiroFalso) {
        this.texto = texto;
        this.alternativa = alternativa;
        this.correta = correta;
        this.verdadeiroFalso = verdadeiroFalso;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public EAlternativa getAlternativa() {
        return alternativa;
    }

    public void setAlternativa(EAlternativa alternativa) {
        this.alternativa = alternativa;
    }

    public Boolean getCorreta() {
        return correta;
    }

    public void setCorreta(Boolean correta) {
        this.correta = correta;
    }

    public Boolean getVerdadeiroFalso() {
        return verdadeiroFalso;
    }

    public void setVerdadeiroFalso(Boolean verdadeiroFalso) {
        this.verdadeiroFalso = verdadeiroFalso;
    }
}
