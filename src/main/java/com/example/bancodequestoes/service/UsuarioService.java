package com.example.bancodequestoes.service;

import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.dto.UsuarioDTO;
import com.example.bancodequestoes.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// autor lincon


@Service
public class UsuarioService {

    /**
     * Repository de Usuario.
     */
    private UsuarioRepository usuarioRepository;

    /**
     * Construtor com parâmetros.
     */
    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    /**
     * Busca todas os usuários.
     * @return {@link Page <Usuario>} Retorna todos os usuários em uma paginação.
     * Se não existente, retorna vazio.
     */
    public Page<Usuario> BuscarUsuarios(Pageable pageable, Boolean isPageable) {
        List<Usuario> query = usuarioRepository.findAll();
        if (isPageable) {
            return PageableExecutionUtils.getPage(query, pageable, query::size);
        } else {
            return new PageImpl<>(query);
        }
    }

    /**
     * Busca um usuário cujo ID é fornecido.
     * @param id Identificador único do Perfil do tipo Long.
     */
    public Usuario BuscarUsuarioId(Long id){
        Optional<Usuario> lista = usuarioRepository.findById(id);
        if (lista.isPresent()) {
            return lista.get();
        }
        throw new RuntimeException();
    }

    /**
     * Salva um usuário quando fornecido nome, email, senha, cpf e formação acadêmica.
     */
    public Usuario salvarUsuario(UsuarioDTO dto) {
        Usuario usuario = new Usuario();
        usuario.setNome(dto.getNome());
        usuario.setEmail(dto.getEmail());
        usuario.setSenha(dto.getSenha());
        usuario.setCpf(dto.getCpf());
        usuario.setFormacaoAcademica(dto.getFormacaoAcademica());
        return usuarioRepository.save(usuario);
    }

    /**
     * Deleta um usuário cujo ID é fornecido.
     * @param id Identificador único do Perfil do tipo Long.
     */
    public Usuario DeletaUsuarioId(Long id){
        Optional<Usuario> lista = usuarioRepository.findById(id);
        if (lista.isPresent()) {
            usuarioRepository.delete(lista.get());
            return lista.get();
        }
        throw new RuntimeException();
    }
}
