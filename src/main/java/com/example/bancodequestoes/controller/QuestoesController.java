package com.example.bancodequestoes.controller;

import com.example.bancodequestoes.model.Questoes;
import com.example.bancodequestoes.model.Respostas;
import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.dto.QuestoesDTO;
import com.example.bancodequestoes.service.QuestoesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "questoes")
public class QuestoesController {

    QuestoesService questoesService;

    @Autowired
    public QuestoesController(QuestoesService questoesService) {
        this.questoesService = questoesService;
    }

    @GetMapping
    public ResponseEntity<Page<Questoes>> BuscaTodasQuestoes(Pageable pageable,
                                                             @RequestParam(value = "isPageable", defaultValue = "false")
                                                                    Boolean isPageable) {
        return new ResponseEntity<>(questoesService.BuscarQuestoes(pageable, isPageable), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public Questoes BuscarQuestoesId(@PathVariable(value = "id") Long id)
    {
        return questoesService.BuscarQuestoesPorId(id);
    }

    @GetMapping(value = "/buscar/{idCategoria}")
    public ResponseEntity<Page<Questoes>> BuscarRespostasPorIdQuestao(Pageable pageable,
                                                                       @RequestParam(value = "isPageable", defaultValue = "false")
                                                                               Boolean isPageable,
                                                                       @PathVariable(name = "idCategoria") Long idCategoria) {
        return new ResponseEntity<>(questoesService.BuscarQuestaoPorCategoria(pageable, isPageable, idCategoria), HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Questoes salvarQuestoes(@RequestBody QuestoesDTO dto) { return questoesService.salvarQuestoes(dto); }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Questoes DeletaQuestao(@PathVariable(value = "id") Long id)
    {
        return questoesService.DeletaQuestao(id);
    }
}
