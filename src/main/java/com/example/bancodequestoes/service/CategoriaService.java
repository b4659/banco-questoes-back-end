package com.example.bancodequestoes.service;

import com.example.bancodequestoes.model.Categoria;
import com.example.bancodequestoes.model.dto.CategoriaDTO;
import com.example.bancodequestoes.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaService {

    private CategoriaRepository categoriaRepository;

    @Autowired
    public CategoriaService(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    public Page<Categoria> BuscarCategoria(Pageable pageable, Boolean isPageable) {
        List<Categoria> query = categoriaRepository.findAll();
        if (isPageable) {
            return PageableExecutionUtils.getPage(query, pageable, query::size);
        } else {
            return new PageImpl<>(query);
        }
    }

    public Categoria salvarCategoria(CategoriaDTO dto) {
        Categoria categoria = new Categoria();
        categoria.setDescricao(dto.getDescricao());
        return categoriaRepository.save(categoria);
    }

    public Categoria DeletaCategoria(Long id) {
        Optional<Categoria> lista = categoriaRepository.findById(id);
        if (lista.isPresent()) {
            categoriaRepository.delete(lista.get());
            return lista.get();
        }
        throw new RuntimeException();
    }
}
