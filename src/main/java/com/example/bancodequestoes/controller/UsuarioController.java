package com.example.bancodequestoes.controller;

//autor lincon

import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.dto.UsuarioDTO;
import com.example.bancodequestoes.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "usuario")
public class UsuarioController {

    /**
     * Instância do Service de Usuario.
     */
    private UsuarioService usuarioService;

    /**
     * Construtor com parâmetros.
     */
    @Autowired
    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping
    public ResponseEntity<Page<Usuario>> BuscaTodosUsuarios(Pageable pageable,
                                                            @RequestParam(value = "isPageable", defaultValue = "false")
                                                            Boolean isPageable) {
        return new ResponseEntity<>(usuarioService.BuscarUsuarios(pageable, isPageable), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public Usuario BuscarUsuarioId(@PathVariable(value = "id") Long id)
    {
        return usuarioService.BuscarUsuarioId(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario salvarUsuario(@RequestBody UsuarioDTO dto) {
        return usuarioService.salvarUsuario(dto);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Usuario DeletaUsuarioId(@PathVariable(value = "id") Long id)
    {
        return usuarioService.DeletaUsuarioId(id);
    }

}
