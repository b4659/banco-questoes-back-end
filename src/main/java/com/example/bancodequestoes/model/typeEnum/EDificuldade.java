package com.example.bancodequestoes.model.typeEnum;

public enum EDificuldade {
    Facil,
    Medio,
    Dificil;
}
