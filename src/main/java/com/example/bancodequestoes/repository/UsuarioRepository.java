package com.example.bancodequestoes.repository;

import com.example.bancodequestoes.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// autor lincon
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
