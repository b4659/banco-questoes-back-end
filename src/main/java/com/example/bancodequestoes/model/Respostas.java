package com.example.bancodequestoes.model;

import com.example.bancodequestoes.model.typeEnum.EAlternativa;
import org.apache.tomcat.jni.Local;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "respostas")
public class Respostas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "texto")
    private String texto;

    @ManyToOne
    @JoinColumn(name = "fk_questao")
    private Questoes questoes;

    @Enumerated(EnumType.STRING)
    @Column(name = "alternativa")
    private EAlternativa alternativa;

    @Column(name = "correta")
    private Boolean correta;

    @Column(name = "v_f")
    private Boolean verdadeiroFalso;

    @CreatedDate
    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;

    @LastModifiedDate
    @Column(name = "data_atualizacao")
    private LocalDateTime dataAtualizacao;

    @Column(name = "deletado")
    private boolean deletado;

    public Respostas() {
    }

    public Respostas(Long id, String texto, Questoes questoes, EAlternativa alternativa, Boolean correta, Boolean verdadeiroFalso, LocalDateTime dataCadastro, LocalDateTime dataAtualizacao, boolean deletado) {
        Id = id;
        this.texto = texto;
        this.questoes = questoes;
        this.alternativa = alternativa;
        this.correta = correta;
        this.verdadeiroFalso = verdadeiroFalso;
        this.dataCadastro = dataCadastro;
        this.dataAtualizacao = dataAtualizacao;
        this.deletado = deletado;
    }

    public Respostas(Long id, String texto, Questoes questoes, EAlternativa alternativa, Boolean correta, Boolean verdadeiroFalso) {
        Id = id;
        this.texto = texto;
        this.questoes = questoes;
        this.alternativa = alternativa;
        this.correta = correta;
        this.verdadeiroFalso = verdadeiroFalso;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Questoes getQuestoes() {
        return questoes;
    }

    public void setQuestoes(Questoes questoes) {
        this.questoes = questoes;
    }

    public EAlternativa getAlternativa() {
        return alternativa;
    }

    public void setAlternativa(EAlternativa alternativa) {
        this.alternativa = alternativa;
    }

    public Boolean getCorreta() {
        return correta;
    }

    public void setCorreta(Boolean correta) {
        this.correta = correta;
    }

    public Boolean getVerdadeiroFalso() {
        return verdadeiroFalso;
    }

    public void setVerdadeiroFalso(Boolean verdadeiroFalso) {
        this.verdadeiroFalso = verdadeiroFalso;
    }

    public LocalDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public LocalDateTime getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public boolean isDeletado() {
        return deletado;
    }

    public void setDeletado(boolean deletado) {
        this.deletado = deletado;
    }

    @Override
    public String toString() {
        return "Respostas{" +
                "Id=" + Id +
                ", texto='" + texto + '\'' +
                ", questoes=" + questoes +
                ", alternativa=" + alternativa +
                ", correta=" + correta +
                ", verdadeiroFalso=" + verdadeiroFalso +
                ", dataCadastro=" + dataCadastro +
                ", dataAtualizacao=" + dataAtualizacao +
                ", deletado=" + deletado +
                '}';
    }
}
