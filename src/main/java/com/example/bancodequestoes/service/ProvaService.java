package com.example.bancodequestoes.service;

import com.example.bancodequestoes.model.Prova;
import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.dto.UsuarioDTO;
import com.example.bancodequestoes.repository.ProvaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProvaService {

    private ProvaRepository provaRepository;

    @Autowired

    public ProvaService(ProvaRepository provaRepository) {
        this.provaRepository = provaRepository;
    }

    /**
     * Busca todas os prova.
     * @return {@link Page <Prova>} Retorna todos os usuários em uma paginação.
     * Se não existente, retorna vazio.
     */
    public Page<Prova> BuscarProva(Pageable pageable, Boolean isPageable) {
        List<Prova> query = provaRepository.findAll();
        if (isPageable) {
            return PageableExecutionUtils.getPage(query, pageable, query::size);
        } else {
            return new PageImpl<>(query);
        }
    }

    /**
     * Busca um prova cujo ID é fornecido.
     * @param id Identificador único do Perfil do tipo Long.
     */
    public Prova BuscarProvaId(Long id){
        Optional<Prova> lista = provaRepository.findById(id);
        if (lista.isPresent()) {
            return lista.get();
        }
        throw new RuntimeException();
    }

    /**
     * Deleta um prova cujo ID é fornecido.
     * @param id Identificador único do Perfil do tipo Long.
     */
    public Prova DeletaProvaId(Long id){
        Optional<Prova> lista = provaRepository.findById(id);
        if (lista.isPresent()) {
            provaRepository.delete(lista.get());
            return lista.get();
        }
        throw new RuntimeException();
    }
}
