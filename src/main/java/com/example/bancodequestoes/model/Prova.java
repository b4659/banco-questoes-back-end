package com.example.bancodequestoes.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "prova")
public class Prova {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "fk_categoria")
    private Categoria categoria;

    @Column(name = "descricao")
    private String descricao;

    @ManyToMany
    @JoinTable(
            name = "questao_prova",
            joinColumns = @JoinColumn(name = "fk_prova"),
            inverseJoinColumns = @JoinColumn(name = "fk_questao"))
    Set<Questoes> listaQuestoes;

    @CreatedDate
    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;

    @LastModifiedDate
    @Column(name = "data_atualizacao")
    private LocalDateTime dataAtualizacao;

    @Column(name = "deletado")
    private boolean deletado;

    public Prova() {
    }

    public Prova(Long id, Categoria categoria, String descricao, Set<Questoes> listaQuestoes, LocalDateTime dataCadastro, LocalDateTime dataAtualizacao, boolean deletado) {
        this.id = id;
        this.categoria = categoria;
        this.descricao = descricao;
        this.listaQuestoes = listaQuestoes;
        this.dataCadastro = dataCadastro;
        this.dataAtualizacao = dataAtualizacao;
        this.deletado = deletado;
    }

    public Prova(Long id, Categoria categoria, String descricao, Set<Questoes> listaQuestoes) {
        this.id = id;
        this.categoria = categoria;
        this.descricao = descricao;
        this.listaQuestoes = listaQuestoes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Questoes> getListaQuestoes() {
        return listaQuestoes;
    }

    public void setListaQuestoes(Set<Questoes> listaQuestoes) {
        this.listaQuestoes = listaQuestoes;
    }

    public LocalDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public LocalDateTime getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public boolean isDeletado() {
        return deletado;
    }

    public void setDeletado(boolean deletado) {
        this.deletado = deletado;
    }

    @Override
    public String toString() {
        return "Prova{" +
                "id=" + id +
                ", categoria=" + categoria +
                ", descricao='" + descricao + '\'' +
                ", dataCadastro=" + dataCadastro +
                ", dataAtualizacao=" + dataAtualizacao +
                ", deletado=" + deletado +
                '}';
    }
}
