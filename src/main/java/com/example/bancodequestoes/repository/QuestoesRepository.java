package com.example.bancodequestoes.repository;

import com.example.bancodequestoes.model.Questoes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestoesRepository extends JpaRepository<Questoes, Long> {

    @Query(name = "BuscarQuestaoPorCategoria", nativeQuery = true,
            value = "select * from banco_questoes.questoes where fk_categoria = :idCategoria")
    List<Questoes> BuscarQuestaoPorCategoria(Long idCategoria);
}
