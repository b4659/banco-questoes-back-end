package com.example.bancodequestoes.model.dto;

public class UsuarioDTO {

    private String nome;
    private String cpf;
    private String email;
    private String senha;
    private String formacaoAcademica;

    public UsuarioDTO() {
    }

    public UsuarioDTO(String nome, String cpf, String email, String senha, String formacaoAcademica) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.senha = senha;
        this.formacaoAcademica = formacaoAcademica;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getFormacaoAcademica() {
        return formacaoAcademica;
    }

    public void setFormacaoAcademica(String formacaoAcademica) {
        this.formacaoAcademica = formacaoAcademica;
    }
}
