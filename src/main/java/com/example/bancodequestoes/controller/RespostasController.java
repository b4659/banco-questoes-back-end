package com.example.bancodequestoes.controller;

import com.example.bancodequestoes.model.Questoes;
import com.example.bancodequestoes.model.Respostas;
import com.example.bancodequestoes.model.dto.RespostasDTO;
import com.example.bancodequestoes.service.RespostasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "respostas")
public class RespostasController {

    private RespostasService respostasService;

    @Autowired
    public RespostasController(RespostasService respostasService) { this.respostasService = respostasService; }

    @GetMapping(value = "/buscar/{idQuestao}")
    public ResponseEntity<Page<Respostas>> BuscarRespostasPorIdQuestao(Pageable pageable,
                                                              @RequestParam(value = "isPageable", defaultValue = "false")
                                                                    Boolean isPageable,
                                                              @PathVariable(name = "idQuestao") Long idQuestao) {
        return new ResponseEntity<>(respostasService.BuscarRespostasPorIdQuestao(pageable, isPageable, idQuestao), HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Respostas salvarRespostas(@RequestBody RespostasDTO dto) { return respostasService.salvarRespostas(dto); }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Respostas DeletaRespostas(@PathVariable(value = "id") Long id)
    {
        return respostasService.DeletaRespostas(id);
    }

}
