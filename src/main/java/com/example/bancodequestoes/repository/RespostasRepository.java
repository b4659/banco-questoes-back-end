package com.example.bancodequestoes.repository;

import com.example.bancodequestoes.model.Respostas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RespostasRepository extends JpaRepository<Respostas, Long> {

    @Query(name = "BuscarRespostasPorIdQuestao", nativeQuery = true,
    value = "select * from banco_questoes.respostas where fk_questao = :idQuestao")
    List<Respostas> BuscarRespostasPorIdQuestao(Long idQuestao);
}
