package com.example.bancodequestoes.controller;

import com.example.bancodequestoes.model.Prova;
import com.example.bancodequestoes.model.Questoes;
import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.dto.UsuarioDTO;
import com.example.bancodequestoes.service.ProvaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "prova")
public class ProvaController {

    private ProvaService provaService;

    @Autowired
    public ProvaController(ProvaService provaService) {
        this.provaService = provaService;
    }

    @GetMapping
    public ResponseEntity<Page<Prova>> BuscarTodasProvas(Pageable pageable,
                                                         @RequestParam(value = "isPageable", defaultValue = "false")
                                                                    Boolean isPageable) {
        return new ResponseEntity<>(provaService.BuscarProva(pageable, isPageable), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public Prova BuscarUsuarioId(@PathVariable(value = "id") Long id)
    {
        return provaService.BuscarProvaId(id);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Prova DeleteProvaId(@PathVariable(value = "id") Long id)
    {
        return provaService.DeletaProvaId(id);
    }

}
