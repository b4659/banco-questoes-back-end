package com.example.bancodequestoes.controller;

import com.example.bancodequestoes.model.Categoria;
import com.example.bancodequestoes.model.dto.CategoriaDTO;
import com.example.bancodequestoes.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "categoria")
public class CategoriaController {

    private CategoriaService categoriaService;

    @Autowired
    public CategoriaController(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

    @GetMapping
    public ResponseEntity<Page<Categoria>> BuscarCategoria(Pageable pageable,
                                                              @RequestParam(value = "isPageable", defaultValue = "false")
                                                                     Boolean isPageable) {
        return new ResponseEntity<>(categoriaService.BuscarCategoria(pageable, isPageable), HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categoria salvarCategoria(@RequestBody CategoriaDTO dto) { return categoriaService.salvarCategoria(dto); }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Categoria DeletaCategoria(@PathVariable(value = "id") Long id)
    {
        return categoriaService.DeletaCategoria(id);
    }
}
