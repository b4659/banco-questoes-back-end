package com.example.bancodequestoes.model.dto;

import com.example.bancodequestoes.model.Categoria;
import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.typeEnum.EDificuldade;

public class QuestoesDTO {

    private String questaoDissertativa;
    private String questaoObjetiva;
    private EDificuldade dificuldade;
    private Categoria categoria;
    private Usuario usuario;
    private String anexos;

    public QuestoesDTO() {
    }

    public QuestoesDTO(String questaoDissertativa, String questaoObjetiva, EDificuldade dificuldade, Categoria categoria, Usuario usuario, String anexos) {
        this.questaoDissertativa = questaoDissertativa;
        this.questaoObjetiva = questaoObjetiva;
        this.dificuldade = dificuldade;
        this.categoria = categoria;
        this.usuario = usuario;
        this.anexos = anexos;
    }

    public String getQuestaoDissertativa() {
        return questaoDissertativa;
    }

    public void setQuestaoDissertativa(String questaoDissertativa) {
        this.questaoDissertativa = questaoDissertativa;
    }

    public String getQuestaoObjetiva() {
        return questaoObjetiva;
    }

    public void setQuestaoObjetiva(String questaoObjetiva) {
        this.questaoObjetiva = questaoObjetiva;
    }

    public EDificuldade getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(EDificuldade dificuldade) {
        this.dificuldade = dificuldade;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getAnexos() {
        return anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }
}
