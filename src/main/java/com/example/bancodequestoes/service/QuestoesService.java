package com.example.bancodequestoes.service;

import com.example.bancodequestoes.model.Questoes;
import com.example.bancodequestoes.model.Respostas;
import com.example.bancodequestoes.model.Usuario;
import com.example.bancodequestoes.model.dto.QuestoesDTO;
import com.example.bancodequestoes.repository.QuestoesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestoesService {

    private QuestoesRepository questoesRepository;

    @Autowired
    public QuestoesService(QuestoesRepository questoesRepository) {
        this.questoesRepository = questoesRepository;
    }

    /**
     * Busca todas as questões.
     * @return {@link Page <Questoes>} Retorna todos os questões em uma paginação.
     * Se não existente, retorna vazio.
     */
    public Page<Questoes> BuscarQuestoes(Pageable pageable, Boolean isPageable) {
        List<Questoes> query = questoesRepository.findAll();
        if (isPageable) {
            return PageableExecutionUtils.getPage(query, pageable, query::size);
        } else {
            return new PageImpl<>(query);
        }
    }

    /**
     * Busca uma questão cujo ID é fornecido.
     * @param id Identificador único do Perfil do tipo Long.
     */
    public Questoes BuscarQuestoesPorId(Long id){
        Optional<Questoes> lista = questoesRepository.findById(id);
        if (lista.isPresent()) {
            return lista.get();
        }
        throw new RuntimeException();
    }

    public Page<Questoes> BuscarQuestaoPorCategoria(Pageable pageable, Boolean isPageable, Long idCategoria) {
        List<Questoes> query = questoesRepository.BuscarQuestaoPorCategoria(idCategoria);
        if (isPageable) {
            return PageableExecutionUtils.getPage(query, pageable, query::size);
        } else {
            return new PageImpl<>(query);
        }
    }

    public Questoes salvarQuestoes(QuestoesDTO dto) {
        Questoes questoes = new Questoes();
        questoes.setAnexos(dto.getAnexos());
        questoes.setCategoria(dto.getCategoria());
        questoes.setQuestaoDissertativa(dto.getQuestaoDissertativa());
        questoes.setQuestaoObjetiva(dto.getQuestaoObjetiva());
        questoes.setDificuldade(dto.getDificuldade());
        questoes.setUsuario(dto.getUsuario());
        return questoesRepository.save(questoes);
    }

    public Questoes DeletaQuestao(Long id) {
        Optional<Questoes> lista = questoesRepository.findById(id);
        if (lista.isPresent()) {
            questoesRepository.delete(lista.get());
            return lista.get();
        }
        throw new RuntimeException();
    }
}
