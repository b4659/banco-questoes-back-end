package com.example.bancodequestoes.service;

import com.example.bancodequestoes.model.Questoes;
import com.example.bancodequestoes.model.Respostas;
import com.example.bancodequestoes.model.dto.RespostasDTO;
import com.example.bancodequestoes.repository.RespostasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RespostasService {

    private RespostasRepository respostasRepository;

    @Autowired
    public RespostasService(RespostasRepository respostasRepository) {
        this.respostasRepository = respostasRepository;
    }

    public Page<Respostas> BuscarRespostasPorIdQuestao(Pageable pageable, Boolean isPageable, Long idQuestao) {
        List<Respostas> query = respostasRepository.BuscarRespostasPorIdQuestao(idQuestao);
        if (isPageable) {
            return PageableExecutionUtils.getPage(query, pageable, query::size);
        } else {
            return new PageImpl<>(query);
        }
    }

    public Respostas salvarRespostas(RespostasDTO dto) {
        Respostas respostas = new Respostas();
        respostas.setTexto(dto.getTexto());
        respostas.setAlternativa(dto.getAlternativa());
        respostas.setCorreta(dto.getCorreta());
        respostas.setVerdadeiroFalso(dto.getVerdadeiroFalso());
        return respostasRepository.save(respostas);
    }

    public Respostas DeletaRespostas(Long id) {
        Optional<Respostas> lista = respostasRepository.findById(id);
        if (lista.isPresent()) {
            respostasRepository.delete(lista.get());
            return lista.get();
        }
        throw new RuntimeException();
    }
}
